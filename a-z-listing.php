<?php
/**
 * This template will be provided with the variable $a_z_query which is an A_Z_Listing instance.
 * Template overridden by MB to see if can bend it to my will.
 */
?>
<!-- <div id="letters">
	<div class="az-letters">
		<?php $a_z_query->the_letters(); ?><div class="clear empty"></div>
	</div>
</div> -->
<?php $thisurl = get_permalink(); ?>
<?php if ( $a_z_query->have_letters() ) : ?>
<div id="az-slider">
	<div id="inner-slider">
			<ul>
		<?php
		while ( $a_z_query->have_letters() ) :
			$a_z_query->the_letter();
		?>
			   <?php if ( $a_z_query->have_items() ) : ?>
						    <?php
						    while ( $a_z_query->have_items() ) :
							    $a_z_query->the_item();
						    ?>
							 <?php 
							       $thaturl = $a_z_query->get_the_permalink();
							       if ($thaturl == $thisurl) {
							           echo "<li class='current-menu-item'>";
							           }
							        else {
							            echo "<li>";
							            };
							            ?>
								    <a href="<?php $a_z_query->the_permalink(); ?>"><?php $a_z_query->the_letter_title(); ?></a>
							    </li>
						    <?php endwhile; ?>					
			    <?php endif; ?>
		<?php endwhile; ?>
			</ul>
	</div>
</div>
<?php else : ?>
	<p><?php esc_html_e( 'There are no posts included in this index.', 'a-z-listing' ); ?></p>
<?php
endif;