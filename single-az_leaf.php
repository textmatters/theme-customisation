<?php /* Template Name: az_leaf */ ?>

<?php get_header(); ?>


<div class="wrap">
	<div class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();
			
                $letter = get_field('letter');
                $image_s = get_field('image_s'); ?>
				<?php if (in_category('object')): ?>
				    <div class="cols3 center"><?php echo do_shortcode('[a-z-listing post-type="az_leaf" taxonomy="category" terms="object"]'); ?></div>
				    <?php else : ?>
                    <div class="cols3 center"><?php echo do_shortcode('[a-z-listing post-type="az_leaf" taxonomy="category" terms="subject"]'); ?></div>
                <?php endif ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	            <div class="cols2">
				     <?php if( $image_s ): ?>
				         <div class="image-s">
				             <?php echo $image_s ?>
				         </div>
				     <?php endif; ?>	            
	            </div><!-- .cols2 -->
				<div class="cols1">
				     <?php if( $letter ): ?>
				         <div class="big-letter">
				             <?php echo $letter ?>
				         </div>
				     <?php endif; ?>
				     <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		             <?php twentyseventeen_edit_link( get_the_ID() ); ?>
		             <?php the_content(); ?>				     
				</div><!-- .cols1 -->
</article><!-- #post-## -->

				<?php // If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();