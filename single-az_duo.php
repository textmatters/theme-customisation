<?php /* Template Name: az_duo */ ?>

<?php get_header(); ?>


<div class="wrap">
	<div class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();
			
                $letter = get_field('letter');
                $sub_img = get_field('sub_img');
                $obj_img = get_field('obj_img');
            ?>
				<div class="cols3 center"><?php echo do_shortcode('[a-z-listing post-type="az_duo"]'); ?></div>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	            <div class="cols2">
	                 <div class="subjectdiv">
	                   <a href="<?php the_field('link_to_subject_leaf'); ?>">
	                     <figure>
				             <?php $subArray = get_field('sub_img'); 
				                 $subURL = esc_attr($subArray['url']);
				                 $subCaption = esc_attr($subArray['caption']); ?>
				             <img src="<?php echo $subURL; ?>" />
				             <figcaption><?php echo $subCaption; ?></figcaption>				             
				         </figure> 
				        </a>    
				     </div>
	                 <div class="objectdiv">
	                     <figure>
	                         <a href="<?php the_field('link_to_object_leaf'); ?>">
				             <?php $objArray = get_field('obj_img'); 
				                 $objURL = esc_attr($objArray['url']);
				                 $objCaption = esc_attr($objArray['caption']); ?>
				             <img src="<?php echo $objURL; ?>" />
				             <figcaption><?php echo $objCaption; ?></figcaption>
				             </a>
				         </figure>     
				     </div>
	            </div><!-- .cols2 -->
				<div class="cols1">
				     <?php if( $letter ): ?>
				         <div class="big-letter">
				             <?php echo $letter ?>
				         </div>
				     <?php endif; ?>
		             <?php twentyseventeen_edit_link( get_the_ID() ); ?>
				</div><!-- .cols1 -->
</article><!-- #post-## -->

				<?php // If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();