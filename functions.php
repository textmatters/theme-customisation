<?php
function my_theme_enqueue_styles() {

    $parent_style = 'twentyseventeen-style'; // This is 'twentyseventeen-style' for the Twenty Seventeen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// adding content types here because git

class az_images {

function __construct() {
   add_action('init',array($this,'create_post_type'));
   }
   
function create_post_type() {
  $labels = array(
    'name'               => 'AZimages',
    'singular_name'      => 'AZimage',
    'menu_name'          => 'AZimages',
    'name_admin_bar'     => 'AZimage',
    'add_new'            => 'Add New',
    'add_new_item'       => 'Add New AZimages',
    'new_item'           => 'New AZimages',
    'edit_item'          => 'Edit AZimages',
    'view_item'          => 'View AZimages',
    'all_items'          => 'All AZimages',
    'search_items'       => 'Search AZimages',
    'parent_item_colon'  => 'Parent AZimages',
    'not_found'          => 'No AZimages Found',
    'not_found_in_trash' => 'No AZimages Found in Trash'
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_nav_menus'   => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-admin-appearance',
    'capability_type'     => 'page',
    'hierarchical'        => true,
    'supports'            => array( 'title', 'editor', 'author', 'page-attributes' ),
    'has_archive'         => false,
    'rewrite'             => array( 'slug' => 'az_images' ),
    'post-formats'        => array( 'aside', 'gallery' ),
    'query_var'           => true,
    'taxonomies'          => array('post_tag', 'category' ),
  );

  register_post_type( 'az_images', $args );
  }
}

new az_images();




class az_leaf {

function __construct() {
   add_action('init',array($this,'create_post_type'));
   }
   
function create_post_type() {
  $labels = array(
    'name'               => 'AZleaf',
    'singular_name'      => 'AZleaf',
    'menu_name'          => 'AZleaves',
    'name_admin_bar'     => 'AZleaf',
    'add_new'            => 'Add New',
    'add_new_item'       => 'Add New AZleaves',
    'new_item'           => 'New AZleaves',
    'edit_item'          => 'Edit AZleaves',
    'view_item'          => 'View AZleaves',
    'all_items'          => 'All AZleaves',
    'search_items'       => 'Search AZleaves',
    'parent_item_colon'  => 'Parent AZleaves',
    'not_found'          => 'No AZleaves Found',
    'not_found_in_trash' => 'No AZleaves Found in Trash'
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_nav_menus'   => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-admin-appearance',
    'capability_type'     => 'page',
    'hierarchical'        => true,
    'supports'            => array( 'title', 'editor', 'author', 'page-attributes', 'thumbnail' ),
    'has_archive'         => false,
    'rewrite'             => array( 'slug' => 'az_leaves' ),
    'post-formats'        => array( 'aside', 'gallery' ),
    'query_var'           => true,
    'taxonomies'          => array('post_tag', 'category' ),
  );

  register_post_type( 'az_leaf', $args );
  }
}

new az_leaf();

class az_duo {

function __construct() {
   add_action('init',array($this,'create_post_type'));
   }
   
function create_post_type() {
  $labels = array(
    'name'               => 'AZduo',
    'singular_name'      => 'AZduo',
    'menu_name'          => 'AZduos',
    'name_admin_bar'     => 'AZduo',
    'add_new'            => 'Add New',
    'add_new_item'       => 'Add New AZduos',
    'new_item'           => 'New AZduos',
    'edit_item'          => 'Edit AZduos',
    'view_item'          => 'View AZduos',
    'all_items'          => 'All AZduos',
    'search_items'       => 'Search AZduos',
    'parent_item_colon'  => 'Parent AZduos',
    'not_found'          => 'No AZduos Found',
    'not_found_in_trash' => 'No AZduos Found in Trash'
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_nav_menus'   => true,
    'show_in_menu'        => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-admin-appearance',
    'capability_type'     => 'page',
    'hierarchical'        => true,
    'supports'            => array( 'title', 'editor', 'author', 'page-attributes', 'thumbnail' ),
    'has_archive'         => false,
    'rewrite'             => array( 'slug' => 'az_duos' ),
    'post-formats'        => array( 'aside', 'gallery' ),
    'query_var'           => true,
    'taxonomies'          => array('post_tag', 'category' ),
  );

  register_post_type( 'az_duo', $args );
  }
}

new az_duo();


function child_theme_setup() {
    add_image_size( 'size1', 230, 2000, false );
    add_image_size( 'size2', 340, 3000, false );
    add_image_size( 'size3', 720, 9000, false );
    add_theme_support( 'post-thumbnails', array( 'az_leaf', 'post', 'page', 'az_images', 'az_duo' ) );
}
add_action( 'after_setup_theme', 'child_theme_setup', 11 );
add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );
function custom_image_sizes_choose( $sizes ) {
    $custom_sizes = array(
        'size1' => 'Size 1',
        'size2' => 'Size 2',
        'size3' => 'Size 3'
    );
    return array_merge( $sizes, $custom_sizes );
}
?>