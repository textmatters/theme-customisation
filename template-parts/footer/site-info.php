<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	&copy; The University of Reading <a href="https://www.reading.ac.uk/typography/research/typ-researchcentres.aspx">Centre for Ephemera Studies</a> <span class="social"><?php echo do_shortcode( '[feather_share]' ); ?></span>
</div><!-- .site-info -->