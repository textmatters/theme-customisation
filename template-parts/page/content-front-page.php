<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel ' ); ?> >
  <?php $carousel = get_field('carousel-holder'); ?>
  <div class="wrap">

	<div class="panel-content cols1">
		
          <?php twentyseventeen_edit_link( get_the_ID() ); ?>
			<!-- <div class="entry-content"> -->
				<?php
					/* translators: %s: Name of current post */
					the_content( sprintf(
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
						get_the_title()
					) );
				?>
				<div class="buttonpanel">
				    <button class="outline" type="button">
				        <a href="/az_images/subjects/">View subjects</a>
				    </button>
				    <button class="outline" type="button">
				        <a href="/az_images/objects/">View objects</a>
				    </button>
				    <button class="solid" type="button">
				        <a href="/az_duos/a/">View together</a>
				    </button>
				    
				</div>
			<!-- </div> .entry-content -->

		
	</div><!-- .panel-content -->
        <div class="cols2">
		
        <?php 
            echo do_shortcode('[smartslider3 slider=2]');
        ?> 
		</div><!-- .cols2 -->
  </div><!-- .wrap -->
</article><!-- #post-## -->